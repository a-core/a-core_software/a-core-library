#include <stdio.h>
#include <time.h>
#include "a-core-utils.h"
#include "a-core.h"
#include "acore-uart.h"

// custom _write, allows using printf() over UART
// Writes chars to FIFO at 0x30001000
// There should be _something_ at hardware side to ...
// output this, e.g. UART or simulation $display calls.
int _write(int file, char* ptr, int len) {
    for (int i = 0; i < len; i++) {
        transmit_byte((volatile uint8_t*)(A_CORE_AXI4LUART), (uint8_t) ptr[i]);
    }
    return len;
}

// Get number of clock cycles since processor reset
clock_t (clock)(void) {
    uint64_t mcycle_full = csr_read_mcycle();
    return (clock_t)mcycle_full;
}

// Get instructions retired since processor reset
volatile uint64_t get_instret() {
    uint64_t minstret_full = csr_read_minstret();
    return minstret_full;
}

// Read mcycle CSR (machine cycles)
volatile uint64_t csr_read_mcycle() {
    uint32_t mcycle, mcycleh;
    uint64_t mcycle_full;
    __asm__ volatile ("csrr %0, mcycle" 
                      : "=r" (mcycle)  /* output : register */
                      : /* input : none */
                      : /* clobbers: none */);
    __asm__ volatile ("csrr %0, mcycleh" 
                      : "=r" (mcycleh)  /* output : register */
                      : /* input : none */
                      : /* clobbers: none */);
    mcycle_full = ((uint64_t)mcycleh << 32) | mcycle;
    return mcycle_full;
}

// Read minstret CSR (instruction retired counter)
volatile uint64_t csr_read_minstret() {
    uint32_t minstret, minstreth;
    uint64_t minstret_full;
    __asm__ volatile ("csrr %0, minstret" 
                      : "=r" (minstret)  /* output : register */
                      : /* input : none */
                      : /* clobbers: none */);
    __asm__ volatile ("csrr %0, minstreth" 
                      : "=r" (minstreth)  /* output : register */
                      : /* input : none */
                      : /* clobbers: none */);
    minstret_full = ((uint64_t)minstreth << 32) | minstret;
    return minstret_full;
}

// Disable F extension
void disable_f() {
    csr_clear_bit_misa((uint32_t)(1 << ('f'-'a')));
}

// Enable F extension
void enable_f() {
    csr_set_bit_misa((uint32_t)(1 << ('f'-'a')));
}

// Read misa CSR (Machine ISA)
volatile uint32_t csr_read_misa() {
    uint32_t misa;
    __asm__ volatile ("csrr %0, misa" 
                      : "=r" (misa)  /* output : register */
                      : /* input : none */
                      : /* clobbers: none */);
    return misa;
}

// Set bit in misa CSR (Machine ISA)
void csr_set_bit_misa(uint32_t mask) {
    __asm__ volatile ("csrs misa, %0" 
                      : /* output : none */
                      : "r" (mask) /* input : from register */
                      : /* clobbers: none */);
    return;
}

// Clear bit in misa CSR (Machine ISA)
void csr_clear_bit_misa(uint32_t mask) {
    __asm__ volatile ("csrc misa, %0" 
                      : /* output : none */
                      : "r" (mask) /* input : from register */
                      : /* clobbers: none */);
    return;
}

// Call this if test was succesfull
void test_pass() {
    printf("PASS\n");
    asm("csrsi 0xbc0, 3;");
    for(;;);
}

// Call this if test failed
void test_fail() {
    printf("FAIL\n");
    asm("csrci 0xbc0, 2; csrsi 0xbc0, 1");
    for(;;);
}

// Call this at the end of the test
void test_end() {
    // Writes 1 to mstopsim CSR to indicate sim stop
    asm("csrsi 0xbc0, 1");
    for(;;);
}

// Initializes UART. Call this in the beginning of the program.
// Inputs: threshold - how many a-core clock cycles is one UART byte
void init_uart(volatile uint32_t* base_addr, uint32_t threshold) {
    *((volatile uint32_t*)(base_addr + UART_TX_CLK_THRESH)) = threshold; // baud rate counter thereshold
                       // = core clock rate / uart baud rate
}

// Waits until UART is ready to receive data
// Essentially, it returns when UART FIFO is empty
void wait_for_uart_ready(volatile uint8_t* base_addr) {
    volatile uint8_t* addr = (volatile uint8_t*) A_CORE_AXI4LUART;
    while ( *(volatile uint8_t*)(base_addr+UART_TX_STATUS) & UART_TX_STATUS_TX_BUSY_MASK); // wait for tx_busy to clear
}

// Enable UART (note: UART is enabled by default)
void enable_uart() {
    *((volatile uint8_t*)(A_CORE_AXI4LUART+UART_TX_CTRL)) |= UART_TX_CTRL_TX_EN_MASK;
}

// Disable UART
void disable_uart() {
    *((volatile uint8_t*)(A_CORE_AXI4LUART+UART_TX_CTRL)) &= ~UART_TX_CTRL_TX_EN_MASK;
}

// Transmits one byte over UART
void transmit_byte(volatile uint8_t* base_addr, uint8_t byte) {
    *((volatile uint8_t*)(A_CORE_AXI4LUART+UART_TX_BYTE)) = byte;
}

// init data registers to zero
void gpo_init(volatile uint32_t* base_addr) {
    *base_addr = 0;
}

// index 0 is the least significant bit of a byte
// index 31 is the most significant bit
void gpo_set_bit(volatile uint32_t* addr, int value, int index) {
    if (value) {
        *addr |= 1 << index;
    } else {
        *addr &= ~(1 << index);
    }
}

// Write a word to IO output
void gpo_write(volatile uint32_t* base_addr, uint32_t value) {
    *base_addr = value;
}

// Read the whole IO input
uint32_t gpi_read(volatile uint32_t* base_addr) {
    return *base_addr;
}

// Read one bit from IO input
uint32_t gpi_read_bit(volatile uint32_t* base_addr, int index) {
    uint32_t word = *base_addr;
    return ((word >> index) & 1);
}

// Enforce a delay
void delay(int cycles) {
    uint32_t start_mcycle = csr_read_mcycle();
    uint32_t current_mcycle;
    uint32_t target_mcycle = start_mcycle + cycles;
    do {
        current_mcycle = csr_read_mcycle();
    } while(current_mcycle <= target_mcycle);
}
