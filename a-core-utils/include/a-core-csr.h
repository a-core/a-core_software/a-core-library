#ifndef __ACORE_CSR_H
#define __ACORE_CSR_H

/*- - - - - - - - - - - – CSR MACROS - - - - - - - - - - - - - */

/* NOTE:
 * Macros are structured in the same way as in the RISC-V Instruction Set
 * Manual: Volume II: Privileged CSR Tables in chapter 2.2:
 * https://riscv.org/wp-content/uploads/2019/08/riscv-privileged-20190608-1.pdf
*/

/* Unprivileged Floating-Point CSRs */
#define CSR_FFLAGS          0x001
#define CSR_FRM             0x002
#define CSR_FCSR            0x003

/* Unprivileged Counter/Timers */
#define CSR_CYCLE           0xC00
#define CSR_TIME            0xC01
#define CSR_INSTRET         0xC02

#define CSR_HPMCOUNTER3     0xC03   
#define CSR_HPMCOUNTER4     0xC04
#define CSR_HPMCOUNTER5     0xC05
#define CSR_HPMCOUNTER6     0xC06
#define CSR_HPMCOUNTER7     0xC07
#define CSR_HPMCOUNTER8     0xC08
#define CSR_HPMCOUNTER9     0xC09
#define CSR_HPMCOUNTER10    0xC0A
#define CSR_HPMCOUNTER11    0xC0B
#define CSR_HPMCOUNTER12    0xC0C
#define CSR_HPMCOUNTER13    0xC0D
#define CSR_HPMCOUNTER14    0xC0E
#define CSR_HPMCOUNTER15    0xC0F
#define CSR_HPMCOUNTER16    0xC10
#define CSR_HPMCOUNTER17    0xC11
#define CSR_HPMCOUNTER18    0xC12
#define CSR_HPMCOUNTER19    0xC13
#define CSR_HPMCOUNTER20    0xC14
#define CSR_HPMCOUNTER21    0xC15
#define CSR_HPMCOUNTER22    0xC16
#define CSR_HPMCOUNTER23    0xC17
#define CSR_HPMCOUNTER24    0xC18
#define CSR_HPMCOUNTER25    0xC19
#define CSR_HPMCOUNTER26    0xC1A
#define CSR_HPMCOUNTER27    0xC1B
#define CSR_HPMCOUNTER28    0xC1C
#define CSR_HPMCOUNTER29    0xC1D
#define CSR_HPMCOUNTER30    0xC1E
#define CSR_HPMCOUNTER31    0xC1F

#define CSR_CYCLEH          0xC80
#define CSR_TIMEH           0xC81
#define CSR_INSTRETH        0xC82

#define CSR_HPMCOUNTER3H    0xC83
#define CSR_HPMCOUNTER4H    0xC84
#define CSR_HPMCOUNTER5H    0xC85
#define CSR_HPMCOUNTER6H    0xC86
#define CSR_HPMCOUNTER7H    0xC87
#define CSR_HPMCOUNTER8H    0xC88
#define CSR_HPMCOUNTER9H    0xC89
#define CSR_HPMCOUNTER10H   0xC8A
#define CSR_HPMCOUNTER11H   0xC8B
#define CSR_HPMCOUNTER12H   0xC8C
#define CSR_HPMCOUNTER13H   0xC8D
#define CSR_HPMCOUNTER14H   0xC8E
#define CSR_HPMCOUNTER15H   0xC8F
#define CSR_HPMCOUNTER16H   0xC90
#define CSR_HPMCOUNTER17H   0xC91
#define CSR_HPMCOUNTER18H   0xC92
#define CSR_HPMCOUNTER19H   0xC93
#define CSR_HPMCOUNTER20H   0xC94
#define CSR_HPMCOUNTER21H   0xC95
#define CSR_HPMCOUNTER22H   0xC96
#define CSR_HPMCOUNTER23H   0xC97
#define CSR_HPMCOUNTER24H   0xC98
#define CSR_HPMCOUNTER25H   0xC99
#define CSR_HPMCOUNTER26H   0xC9A
#define CSR_HPMCOUNTER27H   0xC9B
#define CSR_HPMCOUNTER28H   0xC9C
#define CSR_HPMCOUNTER29H   0xC9D
#define CSR_HPMCOUNTER30H   0xC9E
#define CSR_HPMCOUNTER31H   0xC9F

/* Supervisor Trap Setup */
#define CSR_SSTATUS         0x100
#define CSR_SIE             0x104
#define CSR_STVEC           0x105
#define CSR_SCOUNTEREN      0x106

/* Supervisor Configuration */
#define CSR_SENVCFG         0x10A

/* Supervisor Counter Setup */
#define CSR_SCOUNTINHIBIT   0x120

/* Supervisor Trap Handling */
#define CSR_SSCRATCH        0x140
#define CSR_SEPC            0x141
#define CSR_SCAUSE          0x142
#define CSR_STVAL           0x143
#define CSR_SIP             0x144
#define CSR_SCOUNTOVF       0xDA0

/* Supervisor Protection and Translation */
#define CSR_SATP            0x180

/* Debug/Trace Registers */
#define CSR_SCONTEXT        0x5A8

/* Supervisor State Enable Registers */
#define CSR_SSTATEEN0       0x10C
#define CSR_SSTATEEN1       0x10D
#define CSR_SSTATEEN2       0x10E
#define CSR_SSTATEEN3       0x10F

/* Hypervisor Trap Setup */
#define CSR_HSTATUS         0x600
#define CSR_HEDELEG         0x602
#define CSR_HIDELEG         0x603
#define CSR_HIE             0x604
#define CSR_HCOUNTEREN      0x606
#define CSR_HGEIE           0x607
#define CSR_HEDELEGH        0x612

/* Hypervisor Trap Handling */
#define CSR_HTVAL           0x643
#define CSR_HIP             0x644
#define CSR_HVIP            0x645
#define CSR_HTINST          0x64A
#define CSR_HGEIP           0xE12

/* Hypervisor Configuration */
#define CSR_HENVCFG         0x60A
#define CSR_HENVCFGH        0x61A

/* Hypervisor Protection and Translation */
#define CSR_HGATP           0x680

/* Debug/Trace Registers */
#define CSR_HCONTEXT        0x6A8

/* Hypervisor Counter/Timer Virtualization Registers */
#define CSR_HTIMEDELTA      0x605
#define CSR_HTIMEDELTAH     0x615

/* Hypervisor State Enable Registers */
#define CSR_HSTATEEN0       0x60C
#define CSR_HSTATEEN1       0x60D
#define CSR_HSTATEEN2       0x60E
#define CSR_HSTATEEN3       0x60F

#define CSR_HSTATEEN0H      0x61C
#define CSR_HSTATEEN1H      0x61D
#define CSR_HSTATEEN2H      0x61E
#define CSR_HSTATEEN3H      0x61F

/* Virtual Supervisor Registers */
#define CSR_VSSTATUS        0x200
#define CSR_VSIE            0x204
#define CSR_VSTVEC          0x205
#define CSR_VSSCRATCH       0x240
#define CSR_VSEPC           0x241
#define CSR_VSCAUSE         0x242
#define CSR_VSTVAL          0x243
#define CSR_VSIP            0x244
#define CSR_VSATP           0x280
 
/* Machine Information Registers */
#define CSR_MVENDORID       0xF11
#define CSR_MARCHID         0xF12
#define CSR_MIMPID          0xF13
#define CSR_MHARTID         0xF14
#define CSR_MCONFIGPTR      0xF15

/* Machine Trap Setup */
#define CSR_MSTATUS         0x300
#define CSR_MISA            0x301
#define CSR_MEDELEG         0x302
#define CSR_MIDELEG         0x303
#define CSR_MIE             0x304
#define CSR_MTVEC           0x305
#define CSR_MCOUNTEREN      0x306
#define CSR_MSTATUSH        0x310
#define CSR_MEDELEGH        0x312

/* Machine Trap Handling */
#define CSR_MSCRATCH        0x340
#define CSR_MEPC            0x341
#define CSR_MCAUSE          0x342
#define CSR_MTVAL           0x343
#define CSR_MIP             0x344
#define CSR_MTINST          0x34A
#define CSR_MTVAL2          0x34B

/* Machine Configuration */
#define CSR_MENVCFG         0x30A
#define CSR_MENVCFGH        0x31A
#define CSR_MSECCFG         0x747
#define CSR_MSECCFGH        0x757

/* Machine Memory Protection */
#define CSR_PMPCFG0         0x3A0
#define CSR_PMPCFG1         0x3A1
#define CSR_PMPCFG2         0x3A2
#define CSR_PMPCFG3         0x3A3
#define CSR_PMPCFG4         0x3A4
#define CSR_PMPCFG5         0x3A5
#define CSR_PMPCFG6         0x3A6
#define CSR_PMPCFG7         0x3A7
#define CSR_PMPCFG8         0x3A8
#define CSR_PMPCFG9         0x3A9
#define CSR_PMPCFG10        0x3AA
#define CSR_PMPCFG11        0x3AB
#define CSR_PMPCFG12        0x3AC
#define CSR_PMPCFG13        0x3AD
#define CSR_PMPCFG14        0x3AE
#define CSR_PMPCFG15        0x3AF

#define CSR_PMPADDR0        0x3B0
#define CSR_PMPADDR1        0x3B1
#define CSR_PMPADDR2        0x3B2
#define CSR_PMPADDR3        0x3B3
#define CSR_PMPADDR4        0x3B4
#define CSR_PMPADDR5        0x3B5
#define CSR_PMPADDR6        0x3B6
#define CSR_PMPADDR7        0x3B7
#define CSR_PMPADDR8        0x3B8
#define CSR_PMPADDR9        0x3B9
#define CSR_PMPADDR10       0x3BA
#define CSR_PMPADDR11       0x3BB
#define CSR_PMPADDR12       0x3BC
#define CSR_PMPADDR13       0x3BD
#define CSR_PMPADDR14       0x3BE
#define CSR_PMPADDR15       0x3BF

#define CSR_PMPADDR16       0x3C0
#define CSR_PMPADDR17       0x3C1
#define CSR_PMPADDR18       0x3C2
#define CSR_PMPADDR19       0x3C3
#define CSR_PMPADDR20       0x3C4
#define CSR_PMPADDR21       0x3C5
#define CSR_PMPADDR22       0x3C6
#define CSR_PMPADDR23       0x3C7
#define CSR_PMPADDR24       0x3C8
#define CSR_PMPADDR25       0x3C9
#define CSR_PMPADDR26       0x3CA
#define CSR_PMPADDR27       0x3CB
#define CSR_PMPADDR28       0x3CC
#define CSR_PMPADDR29       0x3CD
#define CSR_PMPADDR30       0x3CE
#define CSR_PMPADDR31       0x3CF

#define CSR_PMPADDR32       0x3D0
#define CSR_PMPADDR33       0x3D1
#define CSR_PMPADDR34       0x3D2
#define CSR_PMPADDR35       0x3D3
#define CSR_PMPADDR36       0x3D4
#define CSR_PMPADDR37       0x3D5
#define CSR_PMPADDR38       0x3D6
#define CSR_PMPADDR39       0x3D7
#define CSR_PMPADDR40       0x3D8
#define CSR_PMPADDR41       0x3D9
#define CSR_PMPADDR42       0x3DA
#define CSR_PMPADDR43       0x3DB
#define CSR_PMPADDR44       0x3DC
#define CSR_PMPADDR45       0x3DD
#define CSR_PMPADDR46       0x3DE
#define CSR_PMPADDR47       0x3DF

#define CSR_PMPADDR48       0x3E0
#define CSR_PMPADDR49       0x3E1
#define CSR_PMPADDR50       0x3E2
#define CSR_PMPADDR51       0x3E3
#define CSR_PMPADDR52       0x3E4
#define CSR_PMPADDR53       0x3E5
#define CSR_PMPADDR54       0x3E6
#define CSR_PMPADDR55       0x3E7
#define CSR_PMPADDR56       0x3E8
#define CSR_PMPADDR57       0x3E9
#define CSR_PMPADDR58       0x3EA
#define CSR_PMPADDR59       0x3EB
#define CSR_PMPADDR60       0x3EC
#define CSR_PMPADDR61       0x3ED
#define CSR_PMPADDR62       0x3EE
#define CSR_PMPADDR63       0x3EF

/* Machine State Enable Registers */
#define CSR_MSTATEEN0       0x30C
#define CSR_MSTATEEN1       0x30D
#define CSR_MSTATEEN2       0x30E
#define CSR_MSTATEEN3       0x30F

#define CSR_MSTATEEN0H      0x31C
#define CSR_MSTATEEN1H      0x31D
#define CSR_MSTATEEN2H      0x31E
#define CSR_MSTATEEN3H      0x31F

/* Machine Non-Maskable Interrupt Handling */
#define CSR_MNSCRATCH       0x740
#define CSR_MNEPC           0x741
#define CSR_MNCAUSE         0x742
#define CSR_MNSTATUS        0x744

/* Machine Counter/Timers */
#define CSR_MCYCLE          0xB00
#define CSR_MINSTRET        0xB02

#define CSR_MHPMCOUNTER3    0xB03
#define CSR_MHPMCOUNTER4    0xB04
#define CSR_MHPMCOUNTER5    0xB05
#define CSR_MHPMCOUNTER6    0xB06
#define CSR_MHPMCOUNTER7    0xB07
#define CSR_MHPMCOUNTER8    0xB08
#define CSR_MHPMCOUNTER9    0xB09
#define CSR_MHPMCOUNTER10   0xB0A
#define CSR_MHPMCOUNTER11   0xB0B
#define CSR_MHPMCOUNTER12   0xB0C
#define CSR_MHPMCOUNTER13   0xB0D
#define CSR_MHPMCOUNTER14   0xB0E
#define CSR_MHPMCOUNTER15   0xB0F

#define CSR_MHPMCOUNTER16   0xB10
#define CSR_MHPMCOUNTER17   0xB11
#define CSR_MHPMCOUNTER18   0xB12
#define CSR_MHPMCOUNTER19   0xB13
#define CSR_MHPMCOUNTER20   0xB14
#define CSR_MHPMCOUNTER21   0xB15
#define CSR_MHPMCOUNTER22   0xB16
#define CSR_MHPMCOUNTER23   0xB17
#define CSR_MHPMCOUNTER24   0xB18
#define CSR_MHPMCOUNTER25   0xB19
#define CSR_MHPMCOUNTER26   0xB1A
#define CSR_MHPMCOUNTER27   0xB1B
#define CSR_MHPMCOUNTER28   0xB1C
#define CSR_MHPMCOUNTER29   0xB1D
#define CSR_MHPMCOUNTER30   0xB1E
#define CSR_MHPMCOUNTER31   0xB1F

#define CSR_MCYCLEH         0xB80
#define CSR_MINSTRETH       0xB82

#define CSR_MHPMCOUNTER3H   0xB83
#define CSR_MHPMCOUNTER4H   0xB84
#define CSR_MHPMCOUNTER5H   0xB85
#define CSR_MHPMCOUNTER6H   0xB86
#define CSR_MHPMCOUNTER7H   0xB87
#define CSR_MHPMCOUNTER8H   0xB88
#define CSR_MHPMCOUNTER9H   0xB89
#define CSR_MHPMCOUNTER10H  0xB8A
#define CSR_MHPMCOUNTER11H  0xB8B
#define CSR_MHPMCOUNTER12H  0xB8C
#define CSR_MHPMCOUNTER13H  0xB8D
#define CSR_MHPMCOUNTER14H  0xB8E
#define CSR_MHPMCOUNTER15H  0xB8F

#define CSR_MHPMCOUNTER16H  0xB90
#define CSR_MHPMCOUNTER17H  0xB91
#define CSR_MHPMCOUNTER18H  0xB92
#define CSR_MHPMCOUNTER19H  0xB93
#define CSR_MHPMCOUNTER20H  0xB94
#define CSR_MHPMCOUNTER21H  0xB95
#define CSR_MHPMCOUNTER22H  0xB96
#define CSR_MHPMCOUNTER23H  0xB97
#define CSR_MHPMCOUNTER24H  0xB98
#define CSR_MHPMCOUNTER25H  0xB99
#define CSR_MHPMCOUNTER26H  0xB9A
#define CSR_MHPMCOUNTER27H  0xB9B
#define CSR_MHPMCOUNTER28H  0xB9C
#define CSR_MHPMCOUNTER29H  0xB9D
#define CSR_MHPMCOUNTER30H  0xB9E
#define CSR_MHPMCOUNTER31H  0xB9F

/* Machine Counter Setup */
#define CSR_MCOUNTINHIBIT   0x320

#define CSR_MHPMEVENT3      0x323
#define CSR_MHPMEVENT4      0x324
#define CSR_MHPMEVENT5      0x325
#define CSR_MHPMEVENT6      0x326
#define CSR_MHPMEVENT7      0x327
#define CSR_MHPMEVENT8      0x328
#define CSR_MHPMEVENT9      0x329
#define CSR_MHPMEVENT10     0x32A
#define CSR_MHPMEVENT11     0x32B
#define CSR_MHPMEVENT12     0x32C
#define CSR_MHPMEVENT13     0x32D
#define CSR_MHPMEVENT14     0x32E
#define CSR_MHPMEVENT15     0x32F

#define CSR_MHPMEVENT16     0x330
#define CSR_MHPMEVENT17     0x331
#define CSR_MHPMEVENT18     0x332
#define CSR_MHPMEVENT19     0x333
#define CSR_MHPMEVENT20     0x334
#define CSR_MHPMEVENT21     0x335
#define CSR_MHPMEVENT22     0x336
#define CSR_MHPMEVENT23     0x337
#define CSR_MHPMEVENT24     0x338
#define CSR_MHPMEVENT25     0x339
#define CSR_MHPMEVENT26     0x33A
#define CSR_MHPMEVENT27     0x33B
#define CSR_MHPMEVENT28     0x33C
#define CSR_MHPMEVENT29     0x33D
#define CSR_MHPMEVENT30     0x33E
#define CSR_MHPMEVENT31     0x33F

#define CSR_MHPMEVENT3H     0x723
#define CSR_MHPMEVENT4H     0x724
#define CSR_MHPMEVENT5H     0x725
#define CSR_MHPMEVENT6H     0x726
#define CSR_MHPMEVENT7H     0x727
#define CSR_MHPMEVENT8H     0x728
#define CSR_MHPMEVENT9H     0x729
#define CSR_MHPMEVENT10H    0x72A
#define CSR_MHPMEVENT11H    0x72B
#define CSR_MHPMEVENT12H    0x72C
#define CSR_MHPMEVENT13H    0x72D
#define CSR_MHPMEVENT14H    0x72E
#define CSR_MHPMEVENT15H    0x72F

#define CSR_MHPMEVENT16H    0x730
#define CSR_MHPMEVENT17H    0x731
#define CSR_MHPMEVENT18H    0x732
#define CSR_MHPMEVENT19H    0x733
#define CSR_MHPMEVENT20H    0x734
#define CSR_MHPMEVENT21H    0x735
#define CSR_MHPMEVENT22H    0x736
#define CSR_MHPMEVENT23H    0x737
#define CSR_MHPMEVENT24H    0x738
#define CSR_MHPMEVENT25H    0x739
#define CSR_MHPMEVENT26H    0x73A
#define CSR_MHPMEVENT27H    0x73B
#define CSR_MHPMEVENT28H    0x73C
#define CSR_MHPMEVENT29H    0x73D
#define CSR_MHPMEVENT30H    0x73E
#define CSR_MHPMEVENT31H    0x73F

/* Debug/Trace Registers (shared with Debug Mode) */
#define CSR_TSELECT         0x7A0
#define CSR_TDATA1          0x7A1
#define CSR_TDATA2          0x7A2
#define CSR_TDATA3          0x7A3
#define CSR_MCONTEXT        0x7A8

/* Debug Mode Registers */
#define CSR_DCSR            0x7B0
#define CSR_DPC             0x7B1
#define CSR_DSCRATCH0       0x7B2
#define CSR_DSCRATCH1       0x7B3

/* - - - - - - - - - - - CSR ACCESSORS - - - - - - - - - - - - */

/* Auxillary to convert input into a literal */
#define __ASM_STR(x) #x // Not to be used anywhere else!

/* NOTE:
 * Here are the CSR accessors as (polymorphic) c-macro functions.
 * Each function takes 1 to 2 arguments. 
 * The target CSR (mandatory) and a Value/Mask (optional).
*/

// Reads CSR
#define csr_read(csr)                                               \
({                                                                  \
    /* Result as proper type */                                     \
    unsigned long __res;                                            \
                                                                    \
    /* CSRR __res, CSR */                                           \
    asm volatile(                                                   \
        "csrr %0, " __ASM_STR(csr)                                  \
        : "=r" (__res)                                              \
        :                                                           \
        : "memory"                                                  \
    );                                                              \
    __res;  /* 'Return' is implicit */                              \
})

// Writes to CSR
#define csr_write(csr, value)                                       \
({                                                                  \
    /* Value as proper type */                                      \
    const unsigned long __val = (unsigned long)(value);             \
                                                                    \
    /* CSRW CSR, __val */                                           \
    asm volatile(                                                   \
        "csrw " __ASM_STR(csr) ", %0"                               \
        :                                                           \
        : "rK" (__val)                                              \
        : "memory"                                                  \
    );                                                              \
})

// Writes to CSR and reads previous value
#define csr_read_write(csr, value)                                  \
({                                                                  \
    /* Value and result as proper type */                           \
    const unsigned long __val = (unsigned long)(value);             \
    unsigned long __res;                                            \
                                                                    \
    /* CSRRW __res, CSR, __val */                                   \
    asm volatile(                                                   \
        "csrrw %0, " __ASM_STR(csr) ", %1"                          \
        : "=r" (__res)                                              \
        : "rK" (__val)                                              \
        : "memory"                                                  \
    );                                                              \
    __res;                                                          \
})

// Set bits of CSR 
#define csr_set_bits(csr, mask)                                     \
({                                                                  \
    /* Value (bitmask) as proper type */                            \
    const unsigned long __mask = (unsigned long)(mask);             \
                                                                    \
    /* CSRS CSR, __mask */                                          \
    asm volatile(                                                   \
        "csrs " __ASM_STR(csr) ", %0"                               \
        :                                                           \
        : "rK" (__mask)                                             \
        : "memory"                                                  \
    );                                                              \
})

// Clear bits of CSR
#define csr_clear_bits(csr, mask)                                   \
({                                                                  \
    /* Value (bitmask) as proper type */                            \
    const unsigned long __mask = (unsigned long)(mask);             \
                                                                    \
    /* CSRC CSR, __mask */                                          \
    asm volatile(                                                   \
        "csrc " __ASM_STR(csr) ", %0"                               \
        :                                                           \
        : "rK" (__mask)                                             \
        : "memory"                                                  \
    );                                                              \
})

// Set bits and reads previous value
#define csr_read_set_bits(csr, mask)                                \
({                                                                  \
    /* Value and bitmask as proper types */                         \
    const unsigned long __mask = (unsigned long)(mask);             \
    unsigned long __res;                                            \
                                                                    \
    /* CSRRS __ret, CSR, __mask */                                  \
    asm volatile(                                                   \
        "csrrs %0, " __ASM_STR(csr) " ,%1"                          \
        : "=r" (__res)                                              \
        : "rK" (__mask)                                             \
        : "memory"                                                  \
    );                                                              \
    __res;  /* 'Return' is implicit */                              \
})

// Clear bits and read previous value
#define csr_read_clear_bits(csr, mask)                              \
({                                                                  \
    /* Value and bitmask as proper types */                         \
    const unsigned long __mask = (unsigned long)(mask);             \
    unsigned long __res;                                            \
                                                                    \
    /* CSRRC __res, CSR, __mask */                                  \
    asm volatile(                                                   \
        "csrrc %0," __ASM_STR(csr) ", %1"                           \
        : "=r" (__res)                                              \
        : "rK" (__mask)                                             \
        : "memory"                                                  \
    );                                                              \
    __res;  /* 'Return' is implicit */                              \
})

#endif /* __ACORE_CSR_H */
