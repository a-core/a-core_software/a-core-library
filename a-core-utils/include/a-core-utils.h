#ifndef __ACORE_UTILS_HH
#define __ACORE_UTILS_HH

#ifdef sim
    #define BAUDRATE 4
#else
    #define BAUDRATE FREQ_CLK_CORE/115200
#endif

#include <stdint.h> // For uint32_t

// custom _write, allows using printf() over UART
int _write(int file, char* ptr, int len);

// Get instructions retired since processor reset
uint64_t get_instret();

// Ends test ...
void  test_end();
void test_pass(); // with PASS
void test_fail(); // with FAIL

// Initializes UART. Call this in the beginning of the program.
void init_uart(volatile uint32_t* base_addr, uint32_t threshold);

// Waits until UART is ready to receive data
void wait_for_uart_ready(volatile uint8_t* base_addr);

// Transmits one byte over UART
void transmit_byte(volatile uint8_t* base_addr, uint8_t byte);

// inits data registers to zero
void gpo_init(volatile uint32_t* base_addr);

// Sets an IO bit at index to value (0/1)
void gpo_set_bit(volatile uint32_t* addr, int value, int index);

// Writes a word to IO output
void gpo_write(volatile uint32_t* base_addr, uint32_t value);

// Reads the whole IO input
uint32_t gpi_read(volatile uint32_t* base_addr);

// Reads one bit from IO input
uint32_t gpi_read_bit(volatile uint32_t* base_addr, int index);

// Enable / disable F extension
void disable_f();
void  enable_f();

// Enforce a delay
void delay(int cycles);

/* 
 * NOTE: The following CSR methods are deprecated,
 * Use the C macro functions in a-core-csr.h instead
*/
uint64_t csr_read_mcycle();
uint64_t csr_read_minstret();
uint32_t csr_read_misa();

void csr_set_bit_misa(uint32_t mask);
void csr_clear_bit_misa(uint32_t mask);

#endif /* __ACORE_UTILS_HH */
