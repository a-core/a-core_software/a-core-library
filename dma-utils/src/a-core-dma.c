#include "a-core-dma.h"

#include "a-core.h"
#include "acore-dma.h"  /* NOTE: For DMA Addresses */

/*- - - - - - - - - - - - - - - - DMA FUNCTIONS - - - - - - - - - - - - - - -*/ 

/* 
 * Auxillary that writes bitfield to some value :
 * - Supports multi-bit values too
 * - Doesn't modify other bits
 * - Can be used to write 0 or 1
 * - addr - Address
 * - offset - bit index of the LSB
 * - mask - bitmask which corresponds to bit location and number of bits
 * - val - value to be set
*/
static void write_bitfield(volatile uint32_t addr, int offset, uint32_t mask, int val) {
    volatile uint32_t old = *((volatile uint32_t*)(addr));
    volatile uint32_t new = (old & ~mask) | (val << offset);
    *((volatile uint32_t*)(addr)) = new;
}

/* Sets the read start and end addresses and the read increment property for DMA read */
void set_dma_read(uint32_t dma_base_addr, uint32_t start_addr, uint32_t end_addr, uint32_t increment) {
    *((volatile uint32_t*)(dma_base_addr + DMA_READ_START_ADDR))  = (uint32_t) start_addr;
    *((volatile uint32_t*)(dma_base_addr + DMA_READ_END_ADDR))    = (uint32_t) end_addr;
    *((volatile uint32_t*)(dma_base_addr + DMA_READ_INCREMENT))   = (uint32_t) increment;
}

/* Sets the write start and end addresses and the read increment property for DMA write */
void set_dma_write(uint32_t dma_base_addr, uint32_t start_addr, uint32_t end_addr, uint32_t increment) {
    *((volatile uint32_t*)(dma_base_addr + DMA_WRITE_START_ADDR))  = (uint32_t) start_addr;
    *((volatile uint32_t*)(dma_base_addr + DMA_WRITE_END_ADDR))    = (uint32_t) end_addr;
    *((volatile uint32_t*)(dma_base_addr + DMA_WRITE_INCREMENT))   = (uint32_t) increment;
}

/* Sets the end condition for DMA */
void set_dma_end_condition(uint32_t dma_base_addr, uint32_t end_condition) {
    // 3rd byte is reserved for end_condition -> can just write the whole byte
    *((volatile uint8_t*)(dma_base_addr + DMA_CONTROL + 2)) = (uint8_t) (end_condition);
}

/* Resets DMA from software */
void reset_dma(uint32_t dma_base_addr) {
    // Auto-clearing
    *((volatile uint32_t*)(dma_base_addr + DMA_CONTROL)) |= (uint32_t) (1 << DMA_CONTROL_SOFT_RESET_OFFSET);
}

/* Starts DMA transaction */
void start_dma(uint32_t dma_base_addr) {
    // Auto-clearing
    *((volatile uint32_t*)(dma_base_addr + DMA_CONTROL))
        |= (uint32_t) (1 << DMA_CONTROL_SOFTWARE_START_OFFSET);
}

/* Waits for DMA done bit */
void wait_for_dma_done(uint32_t dma_base_addr) {
    while (
        !((*((volatile uint32_t*)(dma_base_addr + DMA_STATUS))) & DMA_STATUS_DONE_MASK)
    );
}

