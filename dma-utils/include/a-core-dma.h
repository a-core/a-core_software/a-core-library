#ifndef __ACORE_DMA_H
#define __ACORE_DMA_H

#include <stdint.h> // For uint32_t

/*- - - - - - - - - - - - - - - - DMA FUNCTIONS - - - - - - - - - - - - - - -*/ 

/* Sets the read start and end addresses and the read increment property for DMA read */
void set_dma_read(uint32_t dma_base_addr, uint32_t start_addr, uint32_t end_addr, uint32_t increment);

/* Sets the write start and end addresses and the read increment property for DMA write */
void set_dma_write(uint32_t dma_base_addr, uint32_t start_addr, uint32_t end_addr, uint32_t increment);

/* Sets the end condition for DMA */
void set_dma_end_condition(uint32_t dma_base_addr, uint32_t end_condition);

/* Sets DMA output selector to val */
void set_dma_output_selector(uint32_t dma_base_addr, uint32_t val);

/* Sets DMA input selector to val */
void set_dma_input_selector(uint32_t dma_base_addr, uint32_t val);

/* Resets DMA from software */
void reset_dma(uint32_t dma_base_addr);

/* Starts DMA transaction */
void start_dma(uint32_t dma_base_addr);

/* Waits for a DMA to get status done */
void wait_for_dma_done(uint32_t dma_base_addr);

#endif /* __ACORE_DMA_H */
