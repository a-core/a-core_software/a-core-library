
#ifndef __ACORE_TEST_MACROS_HH
#define __ACORE_TEST_MACROS_HH

#include "a-core.h"
#include "acore-uart.h"
#include "acore-gpio.h"

#ifdef sim
    #define BAUDRATE 4
#else
    #define BAUDRATE FREQ_CLK_CORE/115200
#endif

/*
Generic self-checking test file looks like this:

#include "test_macros.h"

init

test_case_0:
    li TESTNUM, 0
    ...
    bne result, correctResult, err_trap_handler
    call print_ok

test_case_1:
    li TESTNUM, 1
    ...
    bne result, correctResult, err_trap_handler
    call print_ok

j test_pass
*/

#define TESTNUM gp
#define mstopsim 0xbc0

#define saveContext   \
    addi sp, sp, -12; \
    sw s0, 4(sp);     \
    sw s1, 8(sp);     \
    sw ra, 12(sp);

#define loadContext   \
    lw s0, 4(sp);     \
    lw s1, 8(sp);     \
    lw ra, 12(sp);    \
    addi sp, sp, 12;

#define printChar(ascii_num)         \
    jal wait_for_busy;               \
    li t0, A_CORE_AXI4LUART;          \
    li t1, ascii_num;                \
    sb t1, (UART_TX_BYTE)(t0); \

#define printTestNum                 \
    jal wait_for_busy;               \
    li t0, A_CORE_AXI4LUART;          \
    addi t1, TESTNUM, 48;            \
    sb t1, (UART_TX_BYTE)(t0); \
    li t1, 1;                        \
    sb t1, (UART_TX_CTRL)(t0);

#define init                             \
    .section .init, "ax";                \
    .global _start;                      \
    _start:;                             \
        la sp, __stack_top;              \
        la t1, err_trap_handler;         \
        csrw mtvec, t1;                  \
        addi sp, sp, -4;                 \
        /* init UART */;                 \
        li t0, A_CORE_AXI4LUART;          \
        li t1, BAUDRATE;           \
        sw t1, 0(t0);                    \
        j test_start;                    \
    err_trap_handler:;                   \
        call print_err;                  \
        la t2, test_fail;                \
        csrw mepc, t2;                   \
        mret;                            \
    ok_trap_handler:;                    \
        csrr t0, mepc;                   \
        addi t0, t0, 8;                  \
        csrw mepc, t0;                   \
        mret;                            \
    print_ok:;                           \
        saveContext;                     \
        printChar('O');                  \
        printChar('K');                  \
        printChar('\n');                 \
        loadContext;                     \
        ret;                             \
    print_err:;                          \
        saveContext;                     \
        printChar('E');                  \
        printChar('R');                  \
        printChar('R');                  \
        printTestNum;                    \
        printChar('\n')                  \
        loadContext;                     \
        ret;                             \
    wait_for_busy:;                      \
        li t0, 1;                        \
        li t1, A_CORE_AXI4LUART;         \
        lw t2, (UART_TX_STATUS)(t1);     \
        and t3, t0, t2;                  \
        bnez t3, wait_for_busy;          \
        ret;                             \
    test_pass:;                          \
        csrsi mstopsim, 3;               \
        j loop;                          \
    test_fail:;                          \
        csrci mstopsim, 2;               \
        csrsi mstopsim, 1;               \
        j loop;                          \
    loop:;                               \
        j loop;                          \
    test_start:;

#endif /* __ACORE_TEST_MACROS_HH */