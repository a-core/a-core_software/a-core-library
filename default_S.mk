RISCV_PREFIX ?= riscv64-unknown-elf-
GCC = $(RISCV_PREFIX)gcc
OBJDUMP = $(RISCV_PREFIX)objdump
SIZE = $(RISCV_PREFIX)size

SOURCES = $(wildcard *.S)

# Platform to run the program on, e.g. sim, fpga, asic
# Determines UART baud rate
# use "sim" for simulations
PLATFORM ?= sim

# Core clock frequency for physical implementations
FREQ_CLK_CORE ?= 50000000

# Program and data memory specifications
PROGMEM_START ?= 0x00001000
PROGMEM_LENGTH ?= 64K
DATAMEM_START ?= 0x20000000
DATAMEM_LENGTH ?= 64K

CFLAGS ?= -march=$(MARCH) -mabi=$(MABI) -ffreestanding -nostartfiles -O2 -fdata-sections -ffunction-sections --specs=nosys.specs
LDOPTS ?= -T a-core.ld -lc_nano -lgcc -Wl,--gc-sections \
	-Wl,--defsym=PROGMEM_START=$(PROGMEM_START),--defsym=PROGMEM_LENGTH=$(PROGMEM_LENGTH),--defsym=DATAMEM_START=$(DATAMEM_START),--defsym=DATAMEM_LENGTH=$(DATAMEM_LENGTH) \
	-L$(DEFAULT_LINKER_PATH)

# A-Core library PATH (Relative to this Makefile)
A_CORE_LIB_PATH ?= .

# A-Core generated header files
A_CORE_HEADERS_PATH ?= ../../../../../acorechip/chisel/include
A_CORE_HEADERS = $(wildcard $(A_CORE_HEADERS_PATH)/*.h)

# Find library objects in path variable LIBRARIES (Assume they are compiled)
LIBS = $(foreach path, $(LIBRARIES), $(wildcard $(path)/*.o))

# All includes should be added here
INCLUDES ?= $(foreach path, $(LIBRARIES), -I$(path)/include) 	# Auto-add libs
INCLUDES += -I$(A_CORE_HEADERS_PATH)							# Manual add

# Location of linker script
DEFAULT_LINKER_PATH ?= $(A_CORE_LIB_PATH)

.PHONY: all clean dump size

all: size

# Saves the previous platform so that it know to recompile if it changes
.PHONY: force
platform_selection: force
	echo '$(PLATFORM)' | cmp -s - $@ || echo '$(PLATFORM)' > $@

# Compile and link the program pointed by CSOURCES with libraries in LIBS
%.elf: $(SOURCES) $(A_CORE_HEADERS) $(LIBS) a-core.ld platform_selection
	$(GCC) $(SOURCES) $(CFLAGS) -Wall -o $@ $(LIBS) $(LDOPTS) -D$(PLATFORM) -DFREQ_CLK_CORE=$(FREQ_CLK_CORE) $(INCLUDES)

syms: $(PLATFORM).elf
	$(OBJDUMP) -x $< | less

dump: $(PLATFORM).elf
	$(OBJDUMP) -x -d -Mno-aliases $< | less

size: $(PLATFORM).elf
	$(SIZE) $<

clean:
	rm -f *.elf *.o platform_selection
